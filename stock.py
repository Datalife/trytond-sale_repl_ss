# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from tryton_replication import ReplLocalIdSrcMixin


__all__ = ['Location']


class Location(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'stock.location'

    @classmethod
    def _unsent_fields(cls):
        return super(Location, cls)._unsent_fields() + ('left', 'right')
