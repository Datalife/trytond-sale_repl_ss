datalife_sale_repl_ss
=====================

The sale_repl_ss module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_repl_ss/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_repl_ss)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
