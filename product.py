# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from tryton_replication import ReplLocalIdSrcMixin

__all__ = ['Product', 'Template', 'TemplateCategory', 'Category',
    'UomCategory', 'Uom', 'Configuration']


class Product(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'product.product'

    @classmethod
    def get_modify_date(cls, records, names):
        pool = Pool()
        TemplateCategory = pool.get('product.template-product.category')

        template_categories = TemplateCategory.search([
            ('template', 'in', [r.template.id for r in records])])
        products_dates = {}
        for template_category in template_categories:
            for product in template_category.template.products:
                products_dates[product.id] = (product.write_date or
                    product.create_date)

        res = {}
        for r in records:
            product_date = r.write_date or r.create_date
            res[r.id] = max(products_dates.get(
                r.id, None) or product_date, product_date)
        return {'modify_date': res}


class Template(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'product.template'


class TemplateCategory(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'product.template-product.category'


class Category(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'product.category'


class UomCategory(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'product.uom.category'


class Uom(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'product.uom'


class Configuration(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'product.configuration'
