# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from tryton_replication import ReplLocalIdSrcMixin, ReplUUIdMasterDuplexMixin
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction

__all__ = ['Party', 'Company', 'Currency', 'Sale', 'SaleLine', 'Address',
    'PaymentTerm']


class Sale(ReplUUIdMasterDuplexMixin, metaclass=PoolMeta):
    __name__ = 'sale.sale'
    _sequenced_field = 'number'

    @classmethod
    def create(cls, vlist):
        records = super(Sale, cls).create(vlist)
        if Transaction().context.get('synch', False):
            subrecords = [r for r in records if r.state == 'quotation']
            if subrecords:
                cls.set_number(subrecords)
            records = cls.browse(records)
        return records

    @classmethod
    def write(cls, *args):
        actions = iter(args)
        args = []
        subrecords = []
        if Transaction().context.get('synch', False):
            for records, values in zip(actions, actions):
                if 'state' in values and values['state'] == 'quotation':
                    subrecords.extend(
                        [r for r in records if not r.number or
                        not values.get('number', None)])
                args.extend((records, values))
        super(Sale, cls).write(*args)
        if subrecords:
            cls.set_number(subrecords)


class SaleLine(ReplUUIdMasterDuplexMixin, metaclass=PoolMeta):
    __name__ = 'sale.line'


class Currency(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'currency.currency'


class Party(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'party.party'


class Company(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'company.company'
    _force_function_fields = ['active']

    active = fields.Function(
        fields.Boolean('Active'), 'get_active', searcher='search_active')

    @classmethod
    def get_active(cls, records, name):
        pool = Pool()
        User = pool.get('res.user')

        user = User(Transaction().user)
        res = {r.id: False for r in records}
        if user.main_company:
            company_ids = list(map(int, cls.search([
                ('parent', 'child_of', [user.main_company.id])])))
            res.update({cpy: True for cpy in company_ids})
        return res

    @classmethod
    def search_active(cls, name, clause):
        return []

    @classmethod
    def get_modify_date(cls, records, names):
        pool = Pool()
        User = pool.get('res.user')

        user = User(Transaction().user)
        # adapt modify date to take user changes into account
        res = {}
        user_date = user.write_date or user.create_date
        for r in records:
            company_date = r.write_date or r.create_date
            res[r.id] = max(user_date or company_date, company_date)
        return {'modify_date': res}

    @classmethod
    def _get_date_domain(cls, date, field_name):
        pool = Pool()
        User = pool.get('res.user')

        user = User(Transaction().user)
        user_date = getattr(user, field_name, None)
        if not user_date or user_date <= date:
            return []
        company_ids = []
        if user.main_company:
            company_ids = list(map(int, cls.search([
                ('parent', 'child_of', [user.main_company.id])])))
        return company_ids

    @classmethod
    def _to_repl_create_dom(cls, date):
        res = super(Company, cls)._to_repl_create_dom(date)

        company_ids = cls._get_date_domain(date, 'create_date')
        if not company_ids:
            return res
        return ['OR', ('id', 'in', company_ids), res]

    @classmethod
    def _to_repl_write_dom(cls, date):
        res = super(Company, cls)._to_repl_write_dom(date)

        company_ids = cls._get_date_domain(date, 'write_date')
        if not company_ids:
            return res
        return ['OR', ('id', 'in', company_ids), res]


class Address(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'party.address'


class PaymentTerm(ReplLocalIdSrcMixin, metaclass=PoolMeta):
    __name__ = 'account.invoice.payment_term'
