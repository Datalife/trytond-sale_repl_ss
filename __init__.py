# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale
from . import stock
from . import product
from tryton_replication import ReplDeleted, Model, ReplicationLog, Warning_


def register():
    Pool.register(
        sale.Sale,
        sale.SaleLine,
        sale.Party,
        sale.Company,
        sale.Currency,
        sale.Address,
        sale.PaymentTerm,
        stock.Location,
        product.Product,
        product.Template,
        product.TemplateCategory,
        product.Category,
        product.UomCategory,
        product.Uom,
        product.Configuration,
        ReplDeleted,
        Model,
        ReplicationLog,
        Warning_,
        module='sale_repl_ss', type_='model')
